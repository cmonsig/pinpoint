#/***************************************************************************
#    PinPoint
#
#    Creates a QGIS plugin template for use as a starting point in plugin
#    development.
#                             -------------------
#        begin                : 2011-10-20
#        copyright            : (C) 2011 by GeoApt LLC
#        email                : gsherman@geoapt.com
# ***************************************************************************/
#
#/***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************/
# Makefile for a PyQGIS plugin 
#

DOTQGIS = .qgis2
PLUGINNAME=pinpoint

PY_FILES = __init__.py pinpointdialog.py pinpoint.py

EXTRAS = pinpoint.png edit-logo.png metadata.txt

UI_FILES = ui_pinpoint.py

RESOURCE_FILES = resources.py

default: compile

compile: $(UI_FILES) $(RESOURCE_FILES)

%.py : %.qrc
	pyrcc4 -o $@  $<

%.py : %.ui
	pyuic4 -o $@ $<

# The deploy  target only works on unix like operating system where
# the Python plugin directory is located at:
# $HOME/$(DOTQGIS)/python/plugins
deploy: compile
	mkdir -p $(HOME)/$(DOTQGIS)/python/plugins/$(PLUGINNAME)
	cp -vf $(PY_FILES) $(HOME)/$(DOTQGIS)/python/plugins/$(PLUGINNAME)
	cp -vf $(UI_FILES) $(HOME)/$(DOTQGIS)/python/plugins/$(PLUGINNAME)
	cp -vf $(RESOURCE_FILES) $(HOME)/$(DOTQGIS)/python/plugins/$(PLUGINNAME)
	cp -vf $(EXTRAS) $(HOME)/$(DOTQGIS)/python/plugins/$(PLUGINNAME)

# Create a zip package. Requires passing a valid commit or tag as follows:
#   make package VERSION=Version_0.3.2
package: compile
		rm -f pinpoint.zip
		git archive --prefix=pinpoint/ -o pinpoint.zip $(shell git rev-list --max-count=1 HEAD)

clean:
	rm $(UI_FILES) $(RESOURCE_FILES) *.pyc
