"""
/***************************************************************************
 PinPoint
                                 A QGIS plugin
                                 Place a pin with label on the map
                              -------------------
        begin                : 2011-10-29
        copyright            : (C) 2013 by GeoApt LLC
        email                : gsherman@geoapt.com
        edits and updates    : Adapted by Simone Glinz (29.12.2018)
        purpose              : Adapting this plugin to be used to display Google Plus Codes on the map
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from qgis.utils import iface

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
from qgis.gui import * #g
# Initialize Qt resources from file resources.py
# from qgis._gui.QgsWidgetWrapper import layer
from qgis.gui import QgsMapToolEmitPoint, QgsMapToolIdentifyFeature
from .util import *
import olc

import resources
# Import the code for the dialog
from pinpointdialog import PinPointDialog



class PinPoint:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        #g reference to the canvas
        self.canvas = self.iface.mapCanvas()

        #g pin tool
        self.pinTool = QgsMapToolEmitPoint(self.canvas)

        #pin editing tool
        self.pinEditTool = QgsMapToolIdentifyFeature(self.canvas)

        # g boolean for memory layer state
        # Remark: trying to find a more sophisticated check for already existing 'Pins' layer
        # this boolean method is highly error prone..
        # self.have_layer = False


    def initGui(self):
        # Create action that will start plugin configuration
        self.action = QAction(QIcon(":/plugins/pinpoint/pinpoint.png"), \
            "Place a Pin...", self.iface.mainWindow())
        # connect the action to the run method
        QObject.connect(self.action, SIGNAL("triggered()"), self.run)

        # connect signal that the canvas was clicked
        # => the pinTool should only be activated when there is no existing memory layer yet!
        # result = QObject.connect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"), self.place_pin)

        self.editaction = QAction(QIcon(":/plugins/pinpoint/edit-logo.png"), \
            "Edit a Pin...", self.iface.mainWindow())
        # connect the action to the run method
        QObject.connect(self.editaction, SIGNAL("triggered()"), self.run)


        # Add toolbar buttons and menu items
        self.iface.addToolBarIcon(self.action)
        self.iface.addPluginToMenu("&Place a Pin...", self.action)

        self.iface.addToolBarIcon(self.editaction)
        self.iface.addPluginToMenu("&Edit a Pin...", self.editaction)


        # set the default pin label
        self.last_desc = 'pluscode'



    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu("&Place a Pin...",self.action)
        self.iface.removeToolBarIcon(self.action)

        self.iface.removePluginMenu("&Edit a Pin...",self.editaction)
        self.iface.removeToolBarIcon(self.editaction)


    # get the x,y and create the pin
    def place_pin(self, point, button):
        # user might remove the layer so we have to check each time
        # to see if it exists

        # diese Ueberpruefung wurde ausgelagert in die checkexistingpoints Methode
        # if self.have_layer == False:
            #QMessageBox.information(self.iface.mainWindow(),"Memory Provider", "No provider yet")

        ok = False

        (desc,ok) = QInputDialog.getText(
         #  (desc) = QInputDialog.getText(
                self.iface.mainWindow(),
                "Description",
                "Description for pin at %.2f, %.2f" % (float(str(point.x())),float(str(point.y()))),
                QLineEdit.Normal,
                self.last_desc)

        # else:
        if (ok):
            self.last_desc = desc
            self.pluscode = "NULL"

            fc = int(self.provider.featureCount())
            #QMessageBox.information( self.iface.mainWindow(),"Feature Count", str(fc)) #"X,Y = %s,%s" % (str(point.x()),str(point.y())) )
            # create the feature
            feature = QgsFeature()
            feature.setGeometry(QgsGeometry.fromPoint(point))
            feature.setAttributes([fc, desc, point.x(), point.y(), self.pluscode])
            self.pinLayer.startEditing()
            self.pinLayer.addFeature(feature, True)
            self.pinLayer.commitChanges()
            #QMessageBox.information(self.iface.mainWindow(), "Feature/field count", "%s / %s" % (str(self.provider.featureCount()), str(self.provider.fieldCount())))
            #self.pinLayer.reload()
            self.pinLayer.setCacheImage(None)
            self.pinLayer.triggerRepaint()
            self.canvas.refresh()

            # disconnect from click signal
            # ==> This needs to be built in!!
            # QObject.disconnect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"),
            #                   self.place_pin)


            ############################################################################################################
            # </start> This section is copied and slightly adapted from the Lat Long tools
            # PlusCode implementation
            # Source: https://github.com/NationalSecurityAgency/qgis-latlontools-plugin/blob/master/pluscodes.py
            ############################################################################################################


            # Get the field names for the input layer. The will be copied to the output layer with Plus Codes added
            fields = self.pinLayer.pendingFields()
            fieldsout = QgsFields(fields)


            layerCRS = self.pinLayer.crs()
            ppoint = self.pinLayer.dataProvider()

            ppoint.addAttributes(fieldsout)
            self.pinLayer.updateFields()


            # The input to the plus codes conversions requires latitudes and longitudes
            # If the layer is not EPSG:4326 we need to convert it.
            if layerCRS != epsg4326:
                transform = QgsCoordinateTransform(layerCRS, epsg4326)

            iter = self.pinLayer.getFeatures()
            self.pinLayer.startEditing()
            for feature in iter:
                pt = feature.geometry().asPoint()
                if layerCRS != epsg4326:
                    pt = transform.transform(pt)
                try:
                    msg = olc.encode(pt.y(), pt.x())
                    print  "PlusCode: " + msg
                except:
                    msg = ''

                self.pinLayer.changeAttributeValue(feature.id(), 4, msg)

            self.pinLayer.commitChanges()
            self.pinLayer.updateExtents()


            ############################################################################################################
            # </end>
            ############################################################################################################



    # edit an existing pin
    # def edit_pin(self, point, button):
    # def edit_pin(self, fid, idx):
    def edit_pin(self, point, button):
        self.myLayer.dostuff
        self.dlg.show()



    def PinPointlayerExists(self):
        layers = QgsMapLayerRegistry.instance().mapLayers().values()
        for i in layers:
            if i.name() == "Pins":
                return True
                # break
        return False


    # run method that performs all the real work
    def run(self):

        # Check to see if the memory layer has been created and if not,
        # create it

        # if self.have_layer == False:
            #QMessageBox.information(self.iface.mainWindow(),"Memory Provider", "No provider yet")
          if self.PinPointlayerExists() == False:
            # here if clause to check if the user has clicked on an already existing pin
            self.create_pin_layer()
            # set the map tool
            self.canvas.setMapTool(self.pinTool)
          else:

            #    self.display_pin(self)

            # doesn't work..
            # QObject.connect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"), self.display_pin)

            # Not working yet. This is meant to be used for the selectFeature function
            # QObject.disconnect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"), self.selectFeature)

            # remark: this could be adapted to ask the user
            # whether he wants to just display infos about the existing pin when clicked on it or to place another pin.
            # In that case call place_pin() again. This is now possible since create_pin_layer() has been
            # removed out of this method to make it more reusable

            self.create_additional_pins()
            # set the map tool
            self.canvas.setMapTool(self.pinTool)


    def create_pin_layer(self):
      self.pinLayer =  QgsVectorLayer(
              "Point?crs=epsg:3857&field=id:integer&field=description:string(120)&field=x:double&field=y:double&field=pluscode:string(20)&index=yes",
              "Pins",
              "memory")

      self.provider = self.pinLayer.dataProvider()
      self.pinLayer.setDisplayField("description")
      self.pinLayer.setDisplayField("PlusCode")
      QgsMapLayerRegistry.instance().addMapLayer(self.pinLayer)
      result = QObject.connect(self.pinLayer, SIGNAL("layerDeleted()"), self.layer_deleted)
      # self.have_layer = True
      # now that we have a layer to work with call the place_pin() method
      QObject.connect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"), self.place_pin)


    def create_additional_pins(self):
      # QgsMapLayerRegistry.instance().addMapLayer(self.pinLayer)

      # Layer mit Namen 'Pins' aus der Layer Registry auslesen und laden
      for layer in QgsMapLayerRegistry.instance().mapLayers().values():
          if layer.name() == 'Pins':
              # pins_layer = layer
              self.pinLayer = layer
              self.provider = self.pinLayer.dataProvider()
      QObject.connect(self.pinTool, SIGNAL("canvasClicked(const QgsPoint &, Qt::MouseButton)"), self.place_pin)

    # Note: is not working yet
    def edit_existing_pins(self):
        # Layer mit Namen 'Pins' aus der Layer Registry auslesen und laden
        for layer in QgsMapLayerRegistry.instance().mapLayers().values():
            if layer.name() == 'Pins':
                # pins_layer = layer
                self.pinLayer = layer
                self.provider = self.pinLayer.dataProvider()
        QObject.connect(self.pinEditTool, SIGNAL("valueChanged(const QgsPoint &, Qt::MouseButton)"), self.edit_pin)



    def layer_deleted(self):
      self.have_layer = False


