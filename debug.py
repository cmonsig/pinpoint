# source: http://riverscapes.northarrowresearch.com/Development/QGIS/pycharm.html

import os

######################### REMOTE DEBUG #########################
def InitDebug():
    if 'DEBUG_PLUGIN_PINPOINT' in os.environ and os.environ['DEBUG_PLUGIN_PINPOINT'] == "PinPoint":
        import pydevd
        pydevd.settrace('localhost', port=53100, stdoutToServer=True, stderrToServer=True, suspend=False)
######################### /REMOTE DEBUG #########################