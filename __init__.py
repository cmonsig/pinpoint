"""
/***************************************************************************
 PinPoint
                                 A QGIS plugin
 Place a pin with label on the map
                             -------------------
        begin                : 2011-10-29
        copyright            : (C) 2013 by GeoApt LLC
        email                : gsherman@geoapt.com
        edits and updates    : Adapted by Simone Glinz (29.12.2018)
        purpose              : Adapting this plugin to be used to display Google Plus Codes on the map

 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


def name():
    return "Place a pin on the map"

def description():
    return "Creates a pin (marker) with an optional description where you click on the map." 

def version():
    return "Version 2.0"

def icon():
    return "pinpoint.png"

def qgisMinimumVersion():
    return "2.0"

def author():
    return "gsherman"

def email():
    return "gsherman@geoapt.com"

def classFactory(iface):
    # load PinPoint class from file PinPoint
    from pinpoint import PinPoint
    return PinPoint(iface)

#######################################################################
# WARNING: Under no circumstances forget to remove the section below!
# This section causes the plugin to be dependent on a running PyCharm
# Debugger and won't start without it running!
# So remove this section before going prod!
#######################################################################

######################### REMOTE DEBUG #########################
# To activate remote debugging set DEBUG_PLUGIN=RiverscapesToolbar as a QGIS
# Environment variable in Preferences -> System -> Environment
import os
import logging
DEBUG = False

# from pinpoint import debug
# from pinpoint.venv.debug import InitDebug

from .debug import InitDebug

if 'DEBUG_PLUGIN_PINPOINT' in os.environ and os.environ['DEBUG_PLUGIN_PINPOINT'] == "PinPoint":
    debug.InitDebug()
    DEBUG = True
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

######################### /REMOTE DEBUG #########################