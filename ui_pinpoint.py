# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_pinpoint.ui'
#
# Created: Sat Dec 17 22:47:41 2011
#      by: PyQt4 UI code generator 4.8.6
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_PinPoint(object):
    def setupUi(self, PinPoint):
        PinPoint.setObjectName(_fromUtf8("PinPoint"))
        PinPoint.resize(400, 300)
        PinPoint.setWindowTitle(QtGui.QApplication.translate("PinPoint", "PinPoint", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonBox = QtGui.QDialogButtonBox(PinPoint)
        self.buttonBox.setGeometry(QtCore.QRect(30, 240, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))

        self.retranslateUi(PinPoint)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), PinPoint.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), PinPoint.reject)
        QtCore.QMetaObject.connectSlotsByName(PinPoint)

    def retranslateUi(self, PinPoint):
        pass

