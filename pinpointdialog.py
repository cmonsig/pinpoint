"""
/***************************************************************************
 PinPointDialog
                                 A QGIS plugin
 Place a pin with label on the map
                             -------------------
        begin                : 2011-10-29
        copyright            : (C) 2013 by GeoApt LLC
        email                : gsherman@geoapt.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from PyQt4 import QtCore, QtGui
from ui_pinpoint import Ui_PinPoint
# create the dialog for zoom to point
class PinPointDialog(QtGui.QDialog):
    def __init__(self):
        QtGui.QDialog.__init__(self)
        # Set up the user interface from Designer.
        self.ui = Ui_PinPoint()
        self.ui.setupUi(self)
